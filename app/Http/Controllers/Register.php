<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
class Register extends Controller
{
    public function Register(Request $request)
    {
        $data = [];
        $rule = [
          "username" => "required",
          "phone" => "required|numeric",
            "email" => "required|email",
            "password" => "required",
            "repassword" => "required"
        ];
        $message = [
            "username.required" => "Vui lòng nhập Họ và tên",
            "phone.required" => "Vui lòng nhập Số điện thoại",
            "phone.numeric" => "Điện thoại là số, vui lòng nhập lại",
            "email.required" => "Vui lòng nhập địa chỉ Emal",
            "email.email" => "Địa chỉ Email có ký tự @, vui lòng nhập lại",
            "password.required" => "Vui lòng nhập Mật khẩu",
            "repassword.required" => "Vui lòng nhập lại Mật khẩu"
        ];
        if($request->isMethod("post"))
        {
            $data["post"] = $request->all();
            do{
                $valid = Validator::make($request->all(), $rule,$message);
                if($valid->fails()){
                    $error = $valid->errors()->toArray();
                    $data["error"] = $error;
                    break;
                }
                $insertdata = [
                    'username' => $data['post']['username'],
                    'phone' => $data['post']['phone'],
                    'email' => $data['post']['email'],
                    'password' => $data['post']['password'],
                    'repassword' => $data['post']['repassword'],
                ];
                $danhsach =\App\Models\Register::create($insertdata);
                $data['success'] = "Đã Đăng Ký Thành Công Cho" . $data["post"]["username"] . "Với địa chỉ Email :" . $data["post"]["email"];
            }while(false);
        }
        return view("register.register",["data" => $data]);
    }
}
